let express = require('express');
let app = express();
const port = 8080;


//.get= metodo a ser feito '/' passa qual parametro vai acessar no caso o / representa a raiz, request= quequisiçao response = resposta da requisiçao
// dentro da {} siginifica a funçao q vai ser feita no momento em que o a api for acionada
app.get('/',(request,response)=>{
    // response.send envia a resposta de volta para a pessoa q fez a requisiçao
    response.send("{message: metodo get principal}")
});

//recurso funcionario
app.get('/funcionario',(request,response)=>{
    // salvando tudo que vim da url na variavel obj
    let obj = request.query;
    //pego o parametro passado na url q desejo acessa e salo em uma variavel o nome tem q ser exatamente da msm forma
    let nome = obj.nome
    let sobrenome = obj.sobrenome

    response.send("{message: metodo get funcionario " + nome +" "+ sobrenome + "}")
})

// habilitando o serviço na porta 8080
app.listen(port,function(){
    console.log("Projeto rodando na porta: " + port)
})